package com.hexagonalPOC.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hexagonalPOC.business.EmployeeService;
import com.hexagonalPOC.domain.Employee;

@RestController
@RequestMapping("/employees/")
public class EmployeeControllerAdapter implements EmployeeUIPort{
	
	@Autowired
	private EmployeeService employeeService;
	
	@Override
	public void create(@RequestBody Employee request) {
		employeeService.create(request.name, request.role, request.salary);
	}
	
	@Override
	public Employee view(@PathVariable Integer id) {
		Employee employee = employeeService.view(id);
		return employee;
	}

}
