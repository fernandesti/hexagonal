package com.hexagonalPOC.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hexagonalPOC.application.repository.EmployeeRepositoryPort;
import com.hexagonalPOC.domain.Employee;

@Service
public class EmployeeService {

	
	@Autowired
	private EmployeeRepositoryPort employeeRepositoryPort;
	
	public void create(String name, String role, long salary) {
		employeeRepositoryPort.create(name, role, salary);
	}
	
	public Employee view(Integer userId) {
		return employeeRepositoryPort.getEmployee(userId);
	}
}
