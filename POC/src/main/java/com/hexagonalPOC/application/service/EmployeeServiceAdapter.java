package com.hexagonalPOC.application.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.hexagonalPOC.application.repository.EmployeeRepositoryPort;
import com.hexagonalPOC.domain.Employee;

@Service
public class EmployeeServiceAdapter implements EmployeeRepositoryPort {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional
	@Override
	public void create(String name, String role, long salary) {
		Employee employee = new Employee();
		employee.name = name;
		employee.role = role;
		employee.salary = salary;
		entityManager.persist(employee);
	}
	
	@Override
	public Employee getEmployee(Integer userId) {
		return entityManager.find(Employee.class, userId);
	}

}
