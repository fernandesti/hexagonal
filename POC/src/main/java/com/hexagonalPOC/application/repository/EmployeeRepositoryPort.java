package com.hexagonalPOC.application.repository;

import com.hexagonalPOC.domain.Employee;


public interface EmployeeRepositoryPort {
	
	void create(String name, String role, long salary);
	
	Employee getEmployee(Integer id);
}
